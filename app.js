var app = angular.module('twitterStream', []);
//a truly global variable would be in $rootScope but I chose to make it as a factory, cause its more easy to test this way.
//I have a json up there on this link to mock, you can change it to make it for your/twitter restpoint.
app.factory('endpoint', function() {
  return "http://servotechshipping.com/avialdo_crm/queries/twitter.php?a=";
});