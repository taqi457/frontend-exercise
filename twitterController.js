app.controller('twitterController', function($scope, $http, endpoint) {
    
    var result;
    
   $scope.change = function(){
    if($scope.search !== undefined){
        getStream();  
    }
    };
    
    
    function getStream () {
    $http.get( endpoint + $scope.search)
        .success( 
            function(response) {
                if(response.errors === undefined) {
                    $scope.result = response.statuses;
                } 
                
        });
    
    }
    

});